package com.example.OnlineEducationPlatform.Model;


import javax.persistence.*;

@Entity
@Table(name = "enrollments")
public class Enrollments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int enrollId;
    private String date;


    @ManyToOne
    @JoinColumn(name = "userfk")
    private Users userfk;

    @ManyToOne
    @JoinColumn(name = "coursefk")
    private Courses coursefk;

    public Enrollments() {
    }

    public Enrollments(int enrollId, String date, Users userfk, Courses coursefk) {
        this.enrollId = enrollId;
        this.date = date;
        this.userfk = userfk;
        this.coursefk = coursefk;
    }

    public int getEnrollId() {
        return enrollId;
    }

    public void setEnrollId(int enrollId) {
        this.enrollId = enrollId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Users getUserfk() {
        return userfk;
    }

    public void setUserfk(Users userfk) {
        this.userfk = userfk;
    }

    public Courses getCoursefk() {
        return coursefk;
    }

    public void setCoursefk(Courses coursefk) {
        this.coursefk = coursefk;
    }
}
