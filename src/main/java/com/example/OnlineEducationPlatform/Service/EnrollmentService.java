package com.example.OnlineEducationPlatform.Service;

import com.example.OnlineEducationPlatform.Model.Enrollments;
import com.example.OnlineEducationPlatform.Repository.EnrollmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnrollmentService {
    @Autowired
    EnrollmentRepository enrollmentRepositoryObject;

    public String takeEnrollment(Enrollments enrollmentsObject) {
        enrollmentRepositoryObject.save(enrollmentsObject);
        return "You have successsfully Enrolled the Course";
    }

    public List<Enrollments> displayEnrollments() {
        return enrollmentRepositoryObject.findAll();
    }
}
