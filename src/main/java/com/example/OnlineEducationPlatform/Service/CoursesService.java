package com.example.OnlineEducationPlatform.Service;

import com.example.OnlineEducationPlatform.Model.Courses;
import com.example.OnlineEducationPlatform.Repository.CoursesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoursesService {

    @Autowired
    CoursesRepository coursesRepositoryObject;
    public String addCourses(Courses coursesObject) {
        coursesRepositoryObject.save(coursesObject);
        return "Course added successfully";
    }

    public List<Courses> displayCourses() {
        return coursesRepositoryObject.findAll();
    }

    public void deleteCourse(int courseId) {
        coursesRepositoryObject.deleteById(courseId);
    }

    public void updateCourses(Courses contactsObject, int courseId) {
        Courses fetchedCourse=coursesRepositoryObject.findById(courseId).get();
        if(fetchedCourse!=null){
            coursesRepositoryObject.delete(fetchedCourse);
            coursesRepositoryObject.save(contactsObject);
        }
    }
}
