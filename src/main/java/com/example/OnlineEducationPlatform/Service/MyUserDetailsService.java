package com.example.OnlineEducationPlatform.Service;

import com.example.OnlineEducationPlatform.Model.Users;
import com.example.OnlineEducationPlatform.Repository.UserRepository;
import com.example.OnlineEducationPlatform.Security.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Users users = userRepository.findByUserName(userName);
        if (users == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return new MyUserDetails(users);
    }
}
