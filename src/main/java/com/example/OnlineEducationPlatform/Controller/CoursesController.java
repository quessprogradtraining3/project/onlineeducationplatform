package com.example.OnlineEducationPlatform.Controller;

import com.example.OnlineEducationPlatform.Model.Courses;
import com.example.OnlineEducationPlatform.Service.CoursesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CoursesController {
    @Autowired
    CoursesService coursesServiceObject;

    @PostMapping("/addcourse")
    public String addCourse(@RequestBody Courses coursesObject){
        String stringObject =coursesServiceObject.addCourses(coursesObject);
        return stringObject;
    }

    @GetMapping("/displaycourses")
    public List<Courses> displayCourses(){
        return coursesServiceObject.displayCourses();
    }

    @DeleteMapping("/deletecourse/{courseId}")
    public void deleteCourse(@PathVariable int courseId){
        coursesServiceObject.deleteCourse(courseId);
    }

    @PutMapping("/updatecourse/{courseId}")
    public void updateCourse(@RequestBody Courses contactsObject, @PathVariable int courseId){
        coursesServiceObject.updateCourses(contactsObject,courseId);

    }


}
