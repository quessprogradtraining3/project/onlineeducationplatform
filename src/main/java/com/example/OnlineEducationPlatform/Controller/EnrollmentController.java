package com.example.OnlineEducationPlatform.Controller;

import com.example.OnlineEducationPlatform.Model.Enrollments;
import com.example.OnlineEducationPlatform.Model.Users;
import com.example.OnlineEducationPlatform.Service.EnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EnrollmentController {
    @Autowired
    EnrollmentService enrollmentServiceObj;


    @PostMapping("/takeEnrollment")
    public String takeEnrollment(@RequestBody Enrollments enrollmentsObject){
        String stringObject =enrollmentServiceObj.takeEnrollment(enrollmentsObject);
        return stringObject;
    }



    @GetMapping("/displayenrollments")
    public List<Enrollments> displayEnrollments(){
        return enrollmentServiceObj.displayEnrollments();
    }

    
}
