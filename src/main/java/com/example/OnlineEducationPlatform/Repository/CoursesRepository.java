package com.example.OnlineEducationPlatform.Repository;

import com.example.OnlineEducationPlatform.Model.Courses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoursesRepository extends JpaRepository<Courses,Integer> {

}
