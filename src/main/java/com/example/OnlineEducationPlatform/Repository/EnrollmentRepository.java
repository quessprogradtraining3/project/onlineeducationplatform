package com.example.OnlineEducationPlatform.Repository;

import com.example.OnlineEducationPlatform.Model.Enrollments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnrollmentRepository extends JpaRepository<Enrollments, Integer> {
}
